use {
    crate::{
        board::{Direction, GameHandler},
        square::{Square, Square::Stone},
    },
    cursive::{
        event::{Event, EventResult, Key},
        theme::ColorStyle,
        views::{Button, Dialog, LinearLayout, TextView},
        Cursive, CursiveRunner, XY,
    },
    itertools::Itertools,
};

pub(crate) struct InterfaceHandler {
    pub siv: CursiveRunner<Cursive>,
}

impl InterfaceHandler {
    pub fn new() -> Self {
        let mut siv = CursiveRunner::new(
            Cursive::new(),
            Box::new(cursive_buffered_backend::BufferedBackend::new(
                cursive::backends::try_default().unwrap(),
            )),
        );

        siv.load_toml(include_str!("../colors.toml")).unwrap();

        Self { siv }
    }

    pub fn run(&mut self) {
        initialise(&mut self.siv);
        self.siv.run()
    }
}

fn initialise(siv: &mut Cursive) {
    siv.add_layer(
        Dialog::new()
            .title("Go [围棋]")
            .padding_lrtb(2, 2, 1, 1)
            .content(
                LinearLayout::vertical()
                    .child(Button::new_raw("[ New game ]", |s| {
                        start_game(s, GameHandler::new())
                    }))
                    .child(Button::new_raw("[   Exit   ]", |s| s.quit())),
            ),
    );
}

fn start_game(siv: &mut Cursive, board: GameHandler) {
    siv.add_layer(
        Dialog::new().padding_lrtb(2, 2, 1, 1).content(
            LinearLayout::vertical()
                .child(TextView::new(&('a'..='s').join("  ")).center())
                .child(
                    LinearLayout::horizontal()
                        .child(
                            TextView::new((1..=19).join(" \n") + " ")
                                .h_align(cursive::align::HAlign::Right),
                        )
                        .child(board)
                        .child(TextView::new((1..=19).join("\n"))),
                )
                .child(TextView::new(&('a'..='s').join("  ")).center()),
        ),
    );
}

impl cursive::view::View for GameHandler {
    fn draw(&self, printer: &cursive::Printer) {
        for (i, row) in self.board.0.iter().enumerate() {
            for (j, square) in row.iter().enumerate() {
                let text = match (i, j, square) {
                    (_, 0, Stone(_)) => "●─",
                    (_, 18, Stone(_)) => "─●",
                    (_, _, Stone(_)) => "─●─",
                    (0, 0, _) => "╭─",
                    (0, 18, _) => "─╮",
                    (18, 0, _) => "╰─",
                    (18, 18, _) => "─╯",
                    (0, _, _) => "─┬─",
                    (_, 0, _) => "├─",
                    (18, _, _) => "─┴─",
                    (_, 18, _) => "─┤",
                    _ => "─┼─",
                };

                let style = match (square, self.selection.x == j && self.selection.y == i) {
                    (Square::None, false) => ColorStyle::default(),
                    (Stone(c), false) => ColorStyle::front(c.get_rgb()),

                    (Square::None, _) => ColorStyle::back(self.player.get_rgb()),
                    (Stone(c), _) => ColorStyle::new(c.get_rgb(), self.player.get_rgb()),
                };

                printer.with_color(style, |printer| {
                    printer.print(XY::new(if j == 0 { 0 } else { j * 3 - 1 }, i), text)
                })
            }
        }
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        match event {
            Event::Key(Key::Left) | Event::Char('h') => {
                self.selection += Direction::West;
                EventResult::Consumed(None)
            }
            Event::Key(Key::Down) | Event::Char('j') => {
                self.selection += Direction::South;
                EventResult::Consumed(None)
            }
            Event::Key(Key::Up) | Event::Char('k') => {
                self.selection += Direction::North;
                EventResult::Consumed(None)
            }
            Event::Key(Key::Right) | Event::Char('l') => {
                self.selection += Direction::East;
                EventResult::Consumed(None)
            }
            Event::Key(Key::Enter) | Event::Char(' ') => {
                if let Stone(_) = self.board[self.selection] {
                    EventResult::Ignored
                } else {
                    self.board[self.selection] = Stone(self.player.get_turn());
                    EventResult::Consumed(None)
                }
            }
            _ => EventResult::Ignored,
        }
    }

    fn required_size(&mut self, _constraint: cursive::Vec2) -> cursive::Vec2 {
        XY { x: 56, y: 19 }
    }

    fn take_focus(
        &mut self,
        _: cursive::direction::Direction,
    ) -> Result<EventResult, cursive::view::CannotFocus> {
        Ok(EventResult::Consumed(None))
    }
}
