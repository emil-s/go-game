pub mod board;
pub mod interface_handler;
pub mod player;
pub mod point;
pub mod square;

use crate::interface_handler::InterfaceHandler;

fn main() {
    let mut game = InterfaceHandler::new();
    game.run()
}
