use crate::square::{Color, Color::Blue, Color::Red};

#[derive(Clone, Copy)]
pub struct PlayerHandler {
    pub current: Color,
}

impl PlayerHandler {
    pub fn new() -> Self {
        PlayerHandler { current: Blue }
    }

    pub fn get_turn(&mut self) -> Color {
        match self.current {
            Color::Blue => {
                self.current = Red;
                Blue
            }
            Color::Red => {
                self.current = Blue;
                Red
            }
        }
    }

    pub fn get_rgb(&self) -> cursive::theme::Color {
        match self.current {
            Blue => cursive::theme::Color::Rgb(69, 133, 136),
            Red => cursive::theme::Color::Rgb(204, 36, 2),
        }
    }
}
