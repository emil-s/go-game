use {
    crate::{player::PlayerHandler, point::Point, square::Square},
    std::ops::{Index, IndexMut},
};

pub enum Direction {
    North,
    West,
    South,
    East,
}

#[derive(Clone, Copy)]
pub struct GameHandler {
    pub board: Board,
    pub player: PlayerHandler,
    pub selection: Point,
}

impl GameHandler {
    pub fn new() -> Self {
        GameHandler {
            board: Board::new(),
            player: PlayerHandler::new(),
            selection: Point { x: 9, y: 9 },
        }
    }

    pub fn evaluate(&mut self, coord: Point) {
        let piece = self.board[coord];
    }
}

#[derive(Clone, Copy)]
pub struct Board(pub [[Square; 19]; 19]);

impl Board {
    pub fn new() -> Self {
        Board([[Square::None; 19]; 19])
    }
}

impl Index<Point> for Board {
    type Output = Square;
    fn index(&self, index: Point) -> &Self::Output {
        &self.0[index.y][index.x]
    }
}

impl IndexMut<Point> for Board {
    fn index_mut(&mut self, index: Point) -> &mut Self::Output {
        &mut self.0[index.y][index.x]
    }
}
