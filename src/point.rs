use crate::board::Direction;
use std::ops::{Add, AddAssign};

#[derive(Clone, Copy)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    fn add_direction(point: Point, direction: Direction) -> Point {
        match direction {
            Direction::North => Point {
                x: point.x,
                y: if point.y == 0 { 18 } else { point.y - 1 },
            },
            Direction::West => Point {
                x: if point.x == 0 { 18 } else { point.x - 1 },
                y: point.y,
            },
            Direction::South => Point {
                x: point.x,
                y: if point.y == 18 { 0 } else { point.y + 1 },
            },
            Direction::East => Point {
                x: if point.x == 18 { 0 } else { point.x + 1 },
                y: point.y,
            },
        }
    }
}

impl Add<Direction> for Point {
    type Output = Point;
    fn add(self, direction: Direction) -> Point {
        Point::add_direction(self, direction)
    }
}

impl AddAssign<Direction> for Point {
    fn add_assign(&mut self, direction: Direction) {
        *self = Point::add_direction(*self, direction)
    }
}
