use {
    self::Color::{Blue, Red},
    cursive::theme::Color::Rgb,
};

#[derive(Clone, Copy)]
pub enum Color {
    Red,
    Blue,
}

impl Color {
    pub fn get_rgb(&self) -> cursive::theme::Color {
        match self {
            Blue => Rgb(7, 102, 120),
            Red => Rgb(157, 0, 6),
        }
    }
}

#[derive(Clone, Copy)]
pub enum Square {
    Stone(Color),
    None,
}
